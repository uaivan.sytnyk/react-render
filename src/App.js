import React from 'react';
import Page from './Page/Page';
import Store from "./Store";

const App = () => {
  return (
    <div className="App">
      <Store>
        <Page />
      </Store>
    </div>
  );
}

export default App;
