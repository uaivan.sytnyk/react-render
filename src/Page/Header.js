import React from "react";
import ThemedButton from "../components/ThemedButton.js";
import './Header.css'
import { StoreContextConsumer } from "../Store.js";

export class Header extends React.Component {
  render() {
    return (
      <StoreContextConsumer>
        {value =>
          <header className={value.theme === "dark" ? "Header" : "DarkHeader"}>
            <h1>Favourite Movies</h1>
            <ThemedButton />
          </header>
        }
      </StoreContextConsumer>
    );
  }
} 