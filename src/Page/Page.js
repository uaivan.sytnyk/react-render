import React, { Component } from "react";
import { Pagination } from "./Pagination";
import { Header } from "./Header";
import { List } from "./List";
import './Page.css'
import { StoreContextConsumer } from "../Store";
import PopUp from "../components/PopUp";

class Page extends Component {
  constructor(props) {
    super(props);
    this.state = {
      films: [],
      page: 1,
      totalPages: 1,
      selectedFilm: null,
    }
    this.handleFilm = this.handleFilm.bind(this);
    this.fetchPopularFilms = this.fetchPopularFilms.bind(this);
  }

  componentDidMount() {
    this.fetchPopularFilms(this.state.page);
  }

  fetchPopularFilms(page) {
    fetch(`https://api.themoviedb.org/3/movie/popular?api_key=9ab4e0c0c4d3ba62a8ae20bc1aaa38f1&page=${page}`)
      .then(responce => responce.json())
      .then(data => this.setState({ films: data.results, page: page, totalPages: data.total_pages }));
  }

  handleFilm(selectedFilm) {
    this.setState({ selectedFilm });
  }

  render() {
    const { films, selectedFilm } = this.state;
    console.log(selectedFilm);
    return (
      <StoreContextConsumer>
        {value =>
          <div className={value.theme === "dark" ? "Page" : "DarkPage"}>
            <Header />
            <Pagination
              changePage={this.fetchPopularFilms}
              totalPages={this.state.totalPages}
              page={this.state.page}
            />
            <List selectFilm={this.handleFilm} films={films} />
            {selectedFilm && (
              <PopUp date={selectedFilm} closePopUp={() => this.handleFilm(null)} />
            )}
          </div>
        }
      </StoreContextConsumer>
    )
  }
}

export default Page;