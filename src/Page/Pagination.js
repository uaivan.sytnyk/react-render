import React, { Component } from "react";
import './Pagination.css'
import { StoreContextConsumer } from "../Store";

export class Pagination extends Component {
  render() {
    const { page, totalPages, changePage } = this.props;
    return (
      <StoreContextConsumer>
        {value => (
          <div className={value.theme === "dark" ? "Pagination" : "PaginationDark"}>
            <div>Current Page: {page}/{totalPages}</div>

            <button
              onClick={() => changePage(page - 1)}
              disabled={page === 1}
            >
              Previous Page
            </button>

            <button
              onClick={() => changePage(page + 1)}
              //disabled={page === totalPages}
              disabled={page === 500}
            >
              Next Page
            </button>
          </div>
        )}
      </StoreContextConsumer>
    );
  }
}