import React from "react";

const StoreContext = React.createContext();
const StoreContextProvider = StoreContext.Provider;
export const StoreContextConsumer = StoreContext.Consumer;

export default class Store extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      theme: 'dark',
      changeTheme: () => {
        // this.setState({theme: this.state.theme === "dark" ? "light" : "dark"})
        this.setState(({ theme }) => ({
          theme: theme === "dark" ? "light" : "dark"
        }));
      }
    }
  }

  render() {
    return (
      <StoreContextProvider value={this.state}>
        {this.props.children}
      </StoreContextProvider>
    );
  }
}