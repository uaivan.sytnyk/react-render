import React, { Component } from "react";
import './Film.css'
import { StoreContextConsumer } from "../Store";

class Film extends Component {

  render() {
    const { title, poster, overview, children, date, selectFilm } = this.props;
    return (
      <StoreContextConsumer>
        {value => (
          <li className={value.theme === "dark" ? "Film" : "DarkFilm"} >
            <img className='image'
              src={
                poster ? `https://www.themoviedb.org/t/p/w200${poster}`
                  : "https://wellness-psychiatry.com/wp-content/uploads/2016/12/noimage-200x300.jpg"
              }
              alt='poster'
              onClick={() => { selectFilm(date) }}
            />
            <div>
              <h1>{title}</h1>
              <p>{overview}</p>
              {children}
            </div>
          </li>
        )}
      </StoreContextConsumer>
    );
  }
}

export default Film;