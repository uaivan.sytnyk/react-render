import React from "react";
import './PopUp.css'

class PopUp extends React.Component {
  render() {
    const { date, closePopUp } = this.props;
    return (
      <div className="PopUp">
        <span
          class="close-btn"
          onClick={closePopUp}>
          &times;
        </span>
        <p>{date}</p>
      </div>
    )
  }
}

export default PopUp;