import React, { Component } from "react";
import './Switch.css'

class RateSwitch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: true,
    };
    this.toggleShow = this.toggleShow.bind(this);
  }

  toggleShow() {
    this.setState({ toggle: !this.state.toggle });
  }

  render() {
    return (
      <p onClick={this.toggleShow} className="switch">
        {this.state.toggle ? "Show Rate" : `${this.props.popularity} Hide Rate`}
      </p>
    );
  }
};

export default RateSwitch;