import React from "react";
import sunPath from '../images/sun.svg'
import moonPath from '../images/moon.svg'
import { StoreContextConsumer } from "../Store";
import './ThemedButton.css';

class ThemedButton extends React.Component {
  render() {
    return (
      <StoreContextConsumer>
        {value => (
          <div onClick={()=>value.changeTheme()} className={value.theme === "dark" ? "svg" : "svg-dark"}>
              {value.theme ==="dark" ? <img src={moonPath} alt="Moon"/>
                : <img src={sunPath} alt="Sun"/>
              }
          </div>  
        )}   
      </StoreContextConsumer>
    );
  }
}

export default ThemedButton;